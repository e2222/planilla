defmodule Planilla do
  @moduledoc """
  Este módulo permite registrar empleados y poder realizar consultas respecto a los datos ingresados.
  """


  @doc """
  El método main se ejecuta para inicializar el programa.
  """
  def main do
    []
    |>Planilla.menu_principal()
  end



  @doc """
  La funcion `menu_principal` se utiliza para presentar en pantalla el menú principal en el cúal el usuario podra
  seleccionar el rol con el que desea trabajar.
  """
  def menu_principal(lista) do

    case String.trim(IO.gets("\n\nSeleccione un rol \n  1. Digitador \n  2. Consultor \n  0. Salir \n"))  do
      "1" -> Planilla.menu_digitador(lista)
      "2" -> Planilla.menu_consultor(lista)
      "0" -> :init.stop()
      _ -> IO.puts("\n\n Opcion no existe")
            Planilla.menu_principal(lista)
    end

  end


  @doc """
  La función `menu_digitador` se utiliza para presentar al usuario el menú que le premitirá ingresar empleados.
  En está función el usuario debe de especificar el numero de empleados que desea ingresar al sistema.
  """
  def menu_digitador(lista) do

    IO.puts("\n\nMenu digitador")
    String.trim(IO.gets("\n\n Cuantos empleados desea ingresar? \n"))
    |> Integer.parse()
    |> registrar_empleados(lista)


  end



  @doc """
  La función `registrar_empleados` se encarga de registrar los datos de cada empleado que ingresa el usuario.
  Esta función hace uso de la estructura empleado.
  """
  def registrar_empleados({numeroEmpleados, _} , lista) do

    nuevalista =
    for n <- 1..numeroEmpleados do
      %Planilla.Empleado{nombre: String.trim(IO.gets("\n\n Digite el nombre del empleado #{n}:  ")),
                         edad: String.to_integer(String.trim(IO.gets("\n Digite la edad del empleado #{n}:  "))),
                         sexo: String.trim(IO.gets("\n Digite el sexo del empleado #{n}:  ")),
                         profesion: String.trim(IO.gets("\n Digite la profesion del empleado #{n}:  ")),
                         sueldo: Float.parse(String.trim(IO.gets("\n Digite el sueldo del empleado #{n}:  ")))
                        }
    end
    Planilla.menu_principal(lista ++ nuevalista)
  end


  @doc """
  En la función `menu_consultor` se muestran las diferentes opciones de consulta que se pueden ejecutar para
  visualizar datos de los empleados registrados.
  """
  def menu_consultor(lista) do

    case String.trim(IO.gets("\n\nSeleccione un rol \n 1. Lista de empleados \n 2. Lista de nombres \n 3. Lista de edades \n 4. Lista de sexos \n 5. Lista de profesion \n 6. Lista de sueldos \n 7. Presupuesto total \n 0. Regresar a menu principal \n")) do

      "0" -> Planilla.menu_principal(lista)

      "1" -> IO.puts("\n\n Lista de empleados \n #{inspect(lista)}")

      "2" -> IO.puts("\n\n Lista de nombres \n #{inspect(listar_nombres(lista))}") ## Imprime todos los nombre

      "3" -> IO.puts("\n\n Lista de edades \n #{inspect(listar_edades(lista))}") ## Imprime todos las edades

      "4" -> IO.puts("\n\n Lista de sexos \n #{inspect(listar_sexos(lista))} ")  ## Imprime los sexos sumarizados

      "5" -> IO.puts("\n\n Lista de profesiones \n #{inspect(listar_profesiones(lista))}") ## Imprime todas las profesiones

      "6" -> IO.puts("\n\n Lista de sueldos \n #{inspect(listar_sueldos(lista))}") ## Imprime todas las profesiones

      "7" -> IO.puts("\n\n Presupuesto \n #{inspect(calcular_presupuesto(lista))}") ## Imprime todas las profesiones

      _  -> Planilla.menu_consultor(lista)

    end

    Process.sleep(2000)
    Planilla.menu_consultor(lista)
  end


  @doc """
  La función `listar_nombres` se encarga de extraer los nombres de todos los empleados registrados
  y devolverlos en una lista.
  """
  def listar_nombres(lista) do
    for %Planilla.Empleado{nombre: nombre} <- lista, do: nombre
  end


  @doc """
  La función `listar_edades` se encarga de extraer las edades de todos los empleados registrados
  y devolverlos en una lista.
  """
  def listar_edades(lista) do
    for %Planilla.Empleado{edad: edad} <- lista, do: "#{edad}"
  end



  @doc """
  La función `listar_sexos` se encarga de extraer los sexos de todos los empleados registrados
  y devolverlos sumarizados en una lista.
  """
  def listar_sexos(lista) do
    listasexos = for %Planilla.Empleado{sexo: sexo} <- lista, do: sexo
    Enum.frequencies(listasexos)
    |> Map.to_list()
  end


  @doc """
  La función `listar_profesiones` se encarga de extraer las profesiones de todos los empleados registrados
  y devolverlos sumarizados en una lista.
  """
  def listar_profesiones(lista) do
    listaprofesiones = for %Planilla.Empleado{profesion: profesion} <- lista, do: profesion
    Enum.frequencies(listaprofesiones)
    |> Map.to_list()
  end


  @doc """
  La función `listar_sueldos` se encarga de extraer los sueldos de todos los empleados registrados
  y devolverlos en una lista.
  """
  def listar_sueldos(lista) do
    for %Planilla.Empleado{sueldo: {sueldo, _}} <- lista, do: :erlang.float_to_binary(sueldo, [decimals: 2])
  end


  @doc """
  La función `calcular_presupuesto` se encarga de calcular el presupuesto total,
  realizando la suma de los salarios de todos los empleados registrados.
  El tipo de moneda es especificado por el usuario al utilizar esta función.
  """
  def calcular_presupuesto(lista) do

    moneda = case String.trim(IO.gets("\n Seleccione su moneda de preferencia: \n 1. USD \n 2. GBP \n 3. EUR \n ")) do

      "1" -> "USD"
      "2" -> "GBP"
      "3" -> "EUR"

    end

    listasueldos = for %Planilla.Empleado{sueldo: {sueldo, _}} <- lista, do: sueldo
    total = Enum.sum(listasueldos)
    |> Float.ceil(2)
    |> :erlang.float_to_binary([decimals: 2])
    {moneda, total}
  end


end
